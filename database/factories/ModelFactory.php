<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */


/***
*
*   User faker
*
*/
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    $max_users = 30;
    $max_accounts = 25;
    $max_history_years = '-4 years';
    $created = $faker->dateTimeBetween(
               $startDate = '-4 years',
               $endDate = 'now',
               $timezone = date_default_timezone_get()
    );

    return [
        'name' => $faker->unique()->name,
        'email' => $faker->unique()->safeEmail,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone' => $faker->phoneNumber,
        'mailing_address_street' => $faker->streetAddress,
        'mailing_address_city' => $faker->city,
        'mailing_address_state' => $faker->stateAbbr,
        'mailing_address_zip' => $faker->postcode,
        'admin' => 'false',
        'password' => $password ?: $password = bcrypt('password'),
        'remember_token' => str_random(10),
        'created_at' => $created,

        'updated_at' => $faker->dateTimeBetween(
                        $startDate = $created,
                        $max = 'now',
                        $timezone = date_default_timezone_get()
        )
    ];
});

/***
*
*   Account faker
*
*/
$factory->define(App\Models\Account::class, function (Faker\Generator $faker) {
    $max_users = 30;
    $max_accounts = 25;
    $max_history_years = '-4 years';
    $created = $faker->dateTimeBetween(
              $startDate = $max_history_years,
              $endDate = 'now',
              $timezone = date_default_timezone_get()
    );

    return [
        'user_id' => $faker->numberBetween($min = 3, $max = $max_users),
        'type' => $faker->randomElement($array = array('checking', 'savings')),
        'name' => $faker->words($nb = 3, $asText = true),
        'balance' => $faker->randomFloat($nbMaxDecimals = 2, $min = 20.00, $max = 3000.00),
        'status' => 'active',
        'created_at' => $created,
        'updated_at' => $faker->dateTimeBetween(
            $startDate = $created,
                         $max = 'now',
                         $timezone = date_default_timezone_get()
        )
    ];
});


/***
*
*   Transaction faker
*
*/
$factory->define(App\Models\Transaction::class, function (Faker\Generator $faker) {
    $type = $faker->randomElement($array = array(
        'deposit', 'cash_withdraw', 'check_withdraw', 'fine'));

    switch ($type) {
        case 'cash_withdraw':
        case 'check_withdraw':
        case 'fine':
        $amount = $faker->randomFloat($nbMaxDecimals = 2, $min = -299, $max = -5);
        break;

        default:
        $amount = $faker->randomFloat($nbMaxDecimals = 2, $min = 5, $max = 999);
        break;
    }

    $max_users = 30;
    $max_accounts = 25;
    $max_history_years = '-4 years';
    $created = $faker->dateTimeBetween(
              $startDate = $max_history_years,
              $endDate = 'now',
              $timezone = date_default_timezone_get()
    );

    return [
        // 'account_id' => 1,
        'account_id' => $faker->numberBetween($min = 1, $max = $max_accounts),
        'type' => $type,
        'amount' => $amount,
        'source' => $faker->company,
        'source_id' => $faker->word,
        'comments' => $faker->sentence($nbWords = 15, $variableNbWords = true),
        'category_id' => $faker->numberBetween($min = 1, $max = 18),
        'created_at' => $created,
        'updated_at' => $faker->dateTimeBetween(
            $startDate = $created,
                                                $max = 'now',
                                                $timezone = date_default_timezone_get()
        )
    ];
});

/***
*
*   Message faker
*
*/
$factory->define(App\Models\Message::class, function (Faker\Generator $faker) {
    $max_users = 30;
    $max_accounts = 25;
    $max_history_years = '-4 years';
    $created = $faker->dateTimeBetween(
              $startDate = $max_history_years,
              $endDate = 'now',
              $timezone = date_default_timezone_get()
    );

    return [
        'user_id' => $faker->numberBetween($min = 1, $max = 50),
        'subject' => $faker->sentence($nbWords = 5, $variableNbWords = true),
        'body' => $faker->paragraph($nbsentances = 5, $variableNbSentances = true),
        'read' => $faker->randomElement($array = array('true', 'false')),
        'important' => $faker->randomElement($array = array('true', 'false')),
        'active' => $faker->randomElement($array = array('true', 'false')),
        'created_at' => $created,
        'updated_at' => $faker->dateTimeBetween(
            $startDate = $created,
                                                $max = 'now',
                                                $timezone = date_default_timezone_get()
        )
    ];
});
