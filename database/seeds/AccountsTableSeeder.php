<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = DB::table('users')->where('name', 'admin')->first();
        $jamey = DB::table('users')->where('name', 'jamey')->first();

        DB::table('accounts')->insert([
            'user_id' => $admin->id,
            'type' => 'savings',
            'name' => "KBank Main Account",
            'status' => 'active',
            'balance' => '0.00',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('accounts')->insert([
            'user_id' => $jamey->id,
            'type' => 'savings',
            'name' => "Main Savings",
            'status' => 'active',
            'balance' => '1000.00',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('accounts')->insert([
            'user_id' => $jamey->id,
            'type' => 'checking',
            'name' => 'Main Checking',
            'status' => 'active',
            'balance' => '456.78',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        factory('App\Models\Account', 25)->create();
    }
}
