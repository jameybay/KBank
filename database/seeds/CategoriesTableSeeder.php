<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = DB::table('users')->where('name', 'admin')->first();
        DB::table('categorygroups')->insert([
            'user_id' => $admin->id,
            'name' => 'Cash',
            'comments' => 'Cash in hand',
            'taxreport' => 'false',
            'global' => 'true',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('categorygroups')->insert([
            'user_id' => $admin->id,
            'name' => 'Deposits',
            'comments' => 'Incoming',
            'taxreport' => 'true',
            'global' => 'true',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('categorygroups')->insert([
            'user_id' => $admin->id,
            'name' => 'Household',
            'comments' => 'Household items main category',
            'taxreport' => 'false',
            'global' => 'true',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('categorygroups')->insert([
            'user_id' => $admin->id,
            'name' => 'Food',
            'comments' => 'Food items main category',
            'taxreport' => 'false',
            'global' => 'true',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('categorygroups')->insert([
            'user_id' => $admin->id,
            'name' => 'Entertainment',
            'comments' => 'Leisure activities',
            'taxreport' => 'false',
            'global' => 'true',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('categorygroups')->insert([
            'user_id' => $admin->id,
            'name' => 'Automobile',
            'comments' => 'Items and services for your car',
            'taxreport' => 'false',
            'global' => 'true',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('categorygroups')->insert([
            'user_id' => $admin->id,
            'name' => 'Medical',
            'comments' => 'Main Medical category',
            'taxreport' => 'true',
            'global' => 'true',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('categorygroups')->insert([
            'user_id' => $admin->id,
            'name' => 'Taxes',
            'comments' => 'Federal, State, Local taxes and charges',
            'taxreport' => 'true',
            'global' => 'true',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('categorygroups')->insert([
            'user_id' => $admin->id,
            'name' => 'Insurance',
            'comments' => 'Main Insurance category',
            'taxreport' => 'false',
            'global' => 'true',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        /*
            Subcategories
         */
         $deposits = DB::table('categorygroups')->where('name', 'Deposits')->first();
         $household = DB::table('categorygroups')->where('name', 'Household')->first();
         $food = DB::table('categorygroups')->where('name', 'Food')->first();
         $entertainment = DB::table('categorygroups')->where('name', 'Entertainment')->first();
         $car = DB::table('categorygroups')->where('name', 'Automobile')->first();
         $medical = DB::table('categorygroups')->where('name', 'Medical')->first();
         $taxes = DB::table('categorygroups')->where('name', 'Taxes')->first();
         $insurance = DB::table('categorygroups')->where('name', 'Insurance')->first();

         // 1
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $deposits->id,
             'name' => 'Employment',
             'comments' => 'Payment for services',
             'taxreport' => 'true',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 2
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $household->id,
             'name' => 'Cleaning supplies',
             'comments' => 'Supplies for laundry and house cleaning',
             'taxreport' => 'false',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 3
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $food->id,
             'name' => 'Restraunts',
             'comments' => 'Eating out',
             'taxreport' => 'false',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 4
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $food->id,
             'name' => 'Groceries',
             'comments' => 'Grocery food items',
             'taxreport' => 'false',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 5
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $car->id,
             'name' => 'Gas',
             'comments' => 'Gasoline',
             'taxreport' => 'false',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 6
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $car->id,
             'name' => 'Repairs',
             'comments' => 'Repairs for the car',
             'taxreport' => 'false',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 7
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $entertainment->id,
             'name' => 'Movies',
             'comments' => 'Going to the movies',
             'taxreport' => 'false',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 8
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $medical->id,
             'name' => 'Doctors office visit',
             'comments' => 'Visit to your doctors office',
             'taxreport' => 'true',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 9
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $medical->id,
             'name' => 'Hospital',
             'comments' => 'Hospital visit or stay',
             'taxreport' => 'true',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 10
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $medical->id,
             'name' => 'Prescriptions',
             'comments' => 'Prescription medications',
             'taxreport' => 'true',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 11
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $medical->id,
             'name' => 'Non Prescription',
             'comments' => 'Non Prescription medications',
             'taxreport' => 'true',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 12
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $taxes->id,
             'name' => 'Personal Property',
             'comments' => 'Personal property taxes',
             'taxreport' => 'true',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 13
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $taxes->id,
             'name' => 'Federal',
             'comments' => 'Federal Taxes',
             'taxreport' => 'true',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 14
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $taxes->id,
             'name' => 'State',
             'comments' => 'State Taxes',
             'taxreport' => 'true',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 15
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $taxes->id,
             'name' => 'Local',
             'comments' => 'Local Taxes',
             'taxreport' => 'true',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 16
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $insurance->id,
             'name' => 'Health',
             'comments' => 'Health and Dental Insurance',
             'taxreport' => 'false',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         // 17
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $insurance->id,
             'name' => 'Car',
             'comments' => 'Car Insurance',
             'taxreport' => 'false',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
         DB::table('transactioncategories')->insert([
             'user_id' => $admin->id,
             'categorygroup_id' => $deposits->id,
             'name' => 'Transfer',
             'comments' => 'Funds Transfer',
             'taxreport' => 'false',
             'global' => 'true',
             'created_at' => Carbon::now(),
             'updated_at' => Carbon::now(),
         ]);
    }
}
