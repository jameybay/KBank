<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;


class TransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $user = DB::table('users')->where('name', 'jamey')->first();
        $account = DB::table('accounts')->where('user_id', $user->id)->first();
        $transactioncategory = DB::table('transactioncategories')->where('name', 'Employment')->first();
        $start_date = Carbon::now()->subYear();

        //create some known transactions for jamey for test
        DB::table('transactions')->insert([
            'type' => 'deposit',
            // 'amount' => $faker->randomFloat($nbMaxDecimals = 2, $min = 2000, $max = 5000),
            'amount' => 2000.00,
            'account_id' => $account->id,
            'source' => 'NIH',
            'source_id' => '2342good49932-1',
            'comments' => 'Employment',
            'category_id' => $transactioncategory->id,
            'created_at' => $start_date,
            'updated_at' => $start_date,
        ]);

        $transaction_date = $start_date;
        foreach (range(1, 24) as $index) {
            $transaction_date->addDays(15);
            DB::table('transactions')->insert([
                'type' => 'cash_withdrawl',
                'amount' => $faker->randomFloat($nbMaxDecimals = 2, $min = -99, $max = -2),
                'account_id' => $account->id,
                'source' => $faker->company,
                'source_id' => $faker->regexify('[A-Z0-9._%+-]'),
                'comments' => $faker->sentence($nbWords = 15, $variableNbWords = true),
                'category_id' => $faker->numberBetween($min = 1, $max = 18),
                'created_at' => $transaction_date,
                'updated_at' => $transaction_date,
            ]);
        }

        //factory('App\Models\Transactions')->create();
    }
}
