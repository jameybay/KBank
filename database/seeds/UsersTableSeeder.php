<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'first_name' => 'Administrator',
            'last_name' => 'root',
            'phone' => '987-654-3210',
            'mailing_address_street' => 'localhost',
            'mailing_address_city' => 'Computer',
            'mailing_address_state' => 'DC',
            'mailing_address_zip' => '20005',
            'email' => 'admin@mail.com',
            'password' => bcrypt('password'),
            'admin' => 'true',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'name' => 'jamey',
            'first_name' => 'Jamey',
            'last_name' => 'Smartguy',
            'phone' => '987-654-3210',
            'mailing_address_street' => '123 Sane street',
            'mailing_address_city' => 'Reason',
            'mailing_address_state' => 'VT',
            'mailing_address_zip' => '10099',
            'email' => 'jamey@mail.com',
            'password' => bcrypt('password'),
            'admin' => 'false',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        factory('App\Models\User', 30)->create();
    }
}
