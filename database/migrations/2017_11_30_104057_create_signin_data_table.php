<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSigninDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signin_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ipaddress');
            $table->string('browser');
            $table->string('browserversion');
            $table->string('platform');
            $table->string('platformversion');
            $table->string('useremail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signin_data');
    }
}
