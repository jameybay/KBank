<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostTransactionAccountBalanceTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER "post_transaction_account_balance"
        AFTER INSERT ON "transactions"
        BEGIN
            UPDATE accounts
            SET balance  = (round((balance + NEW.amount), 2))
            WHERE accounts.id = NEW.account_id;

            INSERT INTO
            account_history (account_id, transaction_id, created_at, updated_at, balance)
            VALUES (NEW.account_id, NEW.id, NEW.created_at, NEW.updated_at,
            (SELECT balance FROM accounts WHERE id = NEW.account_id));
        END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `post_transaction_account_balance`');
    }
}
