<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostTransactionAccountTransactionCountTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER "post_transaction_account_transaction_count"
        AFTER INSERT ON "transactions"
        BEGIN
            UPDATE accounts
            SET transaction_count  = transaction_count + 1
            WHERE accounts.id = NEW.account_id;
        END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `post_transaction_account_transaction_count`');
    }
}
