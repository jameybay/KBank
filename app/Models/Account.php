<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Account extends Model
{
    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction')
                    ->orderBy('created_at', 'desc');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }



    public function loadTransactions()
    {
        /*
        SELECT t.*, a.id, a.account_id, a.transaction_id, a.balance
        FROM transactions as t, account_history as a
        WHERE a.transaction_id = t.id;
        */

        $transactions = DB::table('transactions')
                            ->join(
                                'account_history',
                                   'account_history.transaction_id',
                                   '=',
                                   'transactions.id'
                            )
                            ->select(DB::raw('transactions.*,
                                              account_history.id,
                                              account_history.account_id,
                                              account_history.transaction_id,
                                              account_history.balance'))
                            ->where('transactions.account_id', '=', $this->id)
                            ->orderBy('transactions.created_at', 'desc')
                            ->get();
        $this->transactions = $transactions;
    }

    public function loadBalanceHistory(int $cut_off_year)
    {
        /*
         SELECT account_id,
                balance,
                strftime('%Y', created_at) as t_year,
                strftime('%m', created_at) as t_month
         FROM account_history
         WHERE account_id = 1
         AND CAST(t_year as INTEGER) > $cut_off_year
         GROUP BY t_month
         ORDER BY t_year desc, t_month desc
        */

        $transactions = DB::table('account_history')
                            ->select(DB::raw("account_id,
                                    balance,
                                    strftime('%Y', created_at) as t_year,
                                    strftime('%m', created_at) as t_month"))
                            ->where([
                                ['account_id', '=', $this->id],
                                ['t_year', '>=', $cut_off_year]
                             ])
                            ->groupBy('t_month')
                            ->latest()
                            ->get();

        $history = [];

        if ($transactions->isEmpty()) {
            $history[0] = 'Nothing';
            $this->history = $history;
            return;
        }

        foreach ($transactions as $month_record) {
            $history[$month_record->t_month . '/' . $month_record->t_year]
                    = $month_record->balance;
        }
        //this month
        //$history[$date->month . '/' . $date->year] = $this->balance;
        $this->history = $history;
    }

    public function loadAccountAllocation()
    {
        /*
        SELECT sum(t.amount), t.category_id, t.account_id, c.id, c.name
        FROM transactions as t, transactioncategories as c
        WHERE t.account_id = 1 and t.category_id = c.id
        GROUP BY t.category_id;
        */

        $transactions = DB::table('transactions')
                            ->join(
                                'transactioncategories',
                                   'transactions.category_id',
                                '=',
                                   'transactioncategories.id'
                            )
                            ->select(DB::raw('sum(amount) as amount,
                                     category_id,
                                     account_id,
                                     transactioncategories.id,
                                     transactioncategories.name'))
                            ->where('account_id', '=', $this->id)
                            ->groupBy('category_id')
                            ->get();

        if ($transactions->isEmpty()) {
            return;
        }

        //['category name', 'amount']
        //amount = abs(amount)
        $category_allocation_expendinture = [];
        $category_allocation_income = [];
        foreach ($transactions as $category) {
            if ($category->amount < 0) {             //expendintures
                $amount = abs($category->amount);
                $category_allocation_expendinture[$category->name] = $amount;
            } else {                                  //incomes
                $amount = $category->amount;
                $category_allocation_income[$category->name] = $amount;
            }
        }
        $this->expendintures = $category_allocation_expendinture;
        $this->incomes = $category_allocation_income;
        return;
    }
}
