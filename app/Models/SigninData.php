<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SigninData extends Model
{
    protected $table = 'signin_data';
    
    protected $fillable = [
        'ipAddress',
        'browser',
        'browserversion',
        'platform',
        'platformversion',
        'useremail'
    ];
}
