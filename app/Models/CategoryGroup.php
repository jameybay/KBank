<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use App\Models\TransactionCategory;


class CategoryGroup extends Model
{
    protected $table = 'categorygroups';

    public function transactioncategories()
    {
        return $this->hasMany('App\Models\TransactionCategory', 'categorygroup_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
