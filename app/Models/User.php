<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
          'email',
          'phone',
          'password',
          'first_name',
          'last_name',
          'mailing_address_street',
          'mailing_address_city',
          'mailing_address_state',
          'mailing_address_zip',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function accounts()
    {
        return $this->hasMany('App\Models\Account');
    }

    public function account($id)
    {
        return $this->accounts->where('id', $id);
    }

    public function messages()
    {
        return $this->belongsToMany('App\Models\Message')
                    ->withPivot('read')
                    ->orderBy('created_at', 'desc');
    }

    public function unreadmessages()
    {
        return $this->belongsToMany('App\Models\Message')
            ->withPivot('read')
            ->wherePivot('read', '=', 'false')
            ->orderBy('created_at', 'desc');
    }

    // public function markmessageread($id)
    // {
    //     $this->belongsToMany('App\Models\Message')
    //     ->withPivot('read')
    //     ->wherePivot('message_id', '=', '$id');
    // }

    public function categories()
    {
        return $this->hasMany('App\Models\Category');
    }

    public function transactioncategories()
    {
        return $this->hasMany('App\Models\TransactionCategory');
    }
}
