<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class TransactionCategory extends Model
{
    protected $table = 'transactioncategories';

    public function group()
    {
        return $this->belongsTo('App\Models\CategoryGroup');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
