<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;
use App\Exceptions;
use App\Classes\ApiJson;
use JWTAuth;
use DB;


class MessageController extends Controller
{
    public function CreateMessage(Request $request)
    {
        $userlist = [];
        $errors = [];

        $message = new Message;
        $message->subject = $request->subject;
        $message->body = $request->body;
        $message->important = $request->important;
        $message->save();

        if($request->users[0] == 0) {
            $userlist = DB::table('users')->pluck('id');
        }
        else {
            $userlist = $request->users;
        }
        foreach ($userlist as $id) {
            try {
                $message->users()->attach($id);
            }
            catch (Exception $e) {
                array_push($errors, 'Sending Message Failed ID: ' + $id);
            }
        }

        if(count($errors) > 0) {
            return ApiJson::ApiResponse($errors, 422);
        }
        else {
            return ApiJson::ApiResponse('Success', 201);
        }
    }

    public function GetMessages(Request $request)
    {
        //get user
        $user = JWTAuth::parseToken()->toUser();

        if(!empty($user)) {
            return response()->json([
                'user' => $user,
                'messages' => $user->messages
            ], 201);
        }

        return ApiJson::ApiResponse("User hhh not found", 422);
    }

    public function GetMessage(Request $request)
    {
        //get user
        $user = JWTAuth::parseToken()->toUser();

        if(!empty($user)) {
            $message = $user->messages->where('id', $request->id)->first();
            return response()->json([
                'message' => $message
            ], 201);
        }

        return ApiJson::ApiResponse("User not found", 422);
    }

    public function MarkMessage(Request $request)
    {
        //get user
        $user = JWTAuth::parseToken()->toUser();

            // create a pivot table model?

        if(!empty($user)) {
            $message = $user->messages->where('id', $request->id)->first();
            if ($message) {
                $message->pivot->read = 'true';
                $message->pivot->save();
                return response()->json([
                    'message' => $message
                ], 201);
            }
        }

        return ApiJson::ApiResponse("Unauthorized", 422);
    }

    public function DeleteMessage(Request $request)
    {
        //get user
        $user = JWTAuth::parseToken()->toUser();

        if(!empty($user)) {
            $message = $user->messages->where('id', $request->id)->first();
            if ($message) {
                $user->messages()->detach($message->id);
                // $message->save();
                return response()->json([
                    'message' => $message
                ], 201);
            }
        }

        return ApiJson::ApiResponse("Unauthorized", 422);
    }

    public function MessageCount(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();
        

        if (!empty($user)) {
           $total = $user->messages->count();
           $unread = $user->unreadmessages->count();

            return response()->json([
                'total' => $total,
                'unread' => $unread
            ], 201);
        }
        return ApiJson::ApiResponse("User not found", 422);
    }
}
