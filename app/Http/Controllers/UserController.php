<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Account;
use App\Exceptions;
use App\Classes\ApiJson;
use Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use DB;


class UserController extends Controller
{
    private function validator_messages()
    {
        //TODO refactor to property

        $messages = [
            'name.required' => 'Missing Name',
            'name.max`' => 'Name maximum of :max exceeded',
            'email.required' => 'Missing Email',
            'email.email' => 'Invalid Email',
            'password.min' => 'Password must be at least :min characters',
            'password.required' => 'Missing Password'
        ];

        return $messages;
    }

    /**
     * [getAuthUser description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAuthUser(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        //if(!empty($user)) {
        if(!empty($user)) {
            return response()->json([
                'user' => $user
            ], 201);
        }

        return ApiJson::ApiResponse("User not found", 422);
    }

    /**
     * [Get the User by id]
     * @param Request $request [The request from the client]
     */
    public function getUser(Request $request)
    {
        $user = User::find($request->id);

        if($user) {
            $user->load('accounts');
            return ApiJson::ApiResponse($user, 200);
        }
        else {
            return ApiJson::ApiErrorResponse("User by id not Found", 404);
        }
    }

    /**
     * [Get Users by range]
     * @param  Request $request [description]
     * @return Users array
     * @return ApiJson status
     */
    public function getUsers(Request $request)
    {
        try {
            $count   = DB::table('users')->count();
            $firstId = DB::table('users')->first();
            $users   = DB::table('users')
                ->whereBetween('id', [
                    $request->begin,
                    $request->end
                ])
                // ->orderBy('created_at', 'desc')
                ->get();
        } catch (Exception $e) {
            return ApiJson::ApiErrorResponse('Internal Error', '500');
        }

        if (count($users)) {
            return ApiJson::ApiResponse([
                'users' => $users,
                'count' => $count,
                'first_record' => $firstId
            ], 200);
        } else {
            return ApiJson::ApiResponse('No Transactions found', 404);
        }

    }

    /**
     * [Signup a new User]
     * @param Request $request [The Request from the client]
     */
    public function CreateUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'password' => 'required|min:6',
            'email' => 'required|email'
            ],
            $this->validator_messages()
        );

        if ($validator->fails()) {
            return ApiJson::ApiErrorResponse($validator->errors(), 400);
        }

        $user = new User;

        $user->name = $request->name;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->mailing_address_street = $request->mailing_address_street;
        $user->mailing_address_city = $request->mailing_address_city;
        $user->mailing_address_state = $request->mailing_address_state;
        $user->mailing_address_zip = $request->mailing_address_zip;

        $status_code = 401;

        try {
            $user->save();
            $status_code = 201;
        }
        catch (\Illuminate\Database\QueryException $e) {
            throw new \App\Exceptions\ApiException($e, "User Already Exists", 422);
        }
        //create the jwt token
        try {
            if(!$token = JWTAuth::fromUser($user)) {
                //bad credendials
                return ApiJson::ApiErrorResponse([
                    "0" => "Can not generate token"
                    ], 400);
            }
        } catch (Exception $e) {
            //token creation failed
            return ApiJson::ApiErrorResponse("Token not created", 500);
        }
        return ApiJson::ApiResponse([
            "status" => "User Created",
            "token" => $token,
            "user" => $user
        ], 200);
    }

    public function SignIn(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'email' => 'required|email'
            ],
            $this->validator_messages()
        );

        if ($validator->fails()) {
            return ApiJson::ApiErrorResponse($validator->errors(), 422);
        }

        $credendials = $request->only('email', 'password');

        //create the jwt token
        try {
            if(!$token = JWTAuth::attempt($credendials)) {
                //bad credendials
                return ApiJson::ApiErrorResponse([
                    "0" => "User/Password combination not found"
                    ], 400);
            }
        } catch (Exception $e) {
            //token creation failed
            return ApiJson::ApiErrorResponse("Token not created", 500);
        }
        //user authenticated
        $user = User::where('email', $request->email)->first();
        return ApiJson::ApiResponse([
            "token" => $token,
            "user" => $user
        ], 200);
    }

    public function SignUp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'password' => 'required|min:6',
            'email' => 'required|email'
            ],
            $this->validator_messages()
        );

        if ($validator->fails()) {
            return ApiJson::ApiErrorResponse($validator->errors(), 400);
        }

        //check email
        $count = User::where('email', $request->email)->count();
        if($count > 0) {
            return ApiJson::ApiResponse("Email already taken", 401);
        }
        // //check user name
        $count = User::where('name', $request->name)->count();
        if($count > 0) {
            return ApiJson::ApiResponse("Name already taken", 409);
        }
        //return response
        return ApiJson::ApiResponse("Success", 201);
    }

    public function ChangePassword(Request $request)
    {
        //get user
        $user = JWTAuth::parseToken()->toUser();
        if(!empty($user)) {
            //check old password
            $old_enc = bcrypt($request->old);
            if($old_enc != $user->password) {
                return ApiJson::ApiResponse("Password mismatch", 403);
            }
            //change password
            $user->password = bcrypt($request->new);

            try {
                $user->save();
            }
            catch(Exception $e) {
                return ApiJson::ApiErrorResponse('Error saving change', 403);
            }
            return ApiJson::ApiResponse("Changed", 201);
        }
        return ApiJson::ApiResponse("Unauthorized", 401);
    }

    public function ChangeInfo(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();
        if(!empty($user)) {
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->mailing_address_street = $request->mailing_address_street;
            $user->mailing_address_city = $request->mailing_address_city;
            $user->mailing_address_state = $request->mailing_address_state;
            $user->mailing_address_zip = $request->mailing_address_zip;
            $user->email = $request->email;
            $user->phone = $request->phone;

            try {
                $user->save();
            }
            catch(Exception $e) {
                return ApiJson::ApiErrorResponse('Error saving change', 403);
            }

            return ApiJson::ApiResponse("Changed", 201);
        }
        return ApiJson::ApiResponse("Unauthorized", 401);
    }
}
