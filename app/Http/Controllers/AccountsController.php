<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Account;
use Exceptions;
use Illuminate\Database\QueryException;
use App\Exceptions\ApiException;
use App\Classes\ApiJson;
use JWTAuth;
use Carbon\Carbon;
use DB;

class AccountsController extends Controller
{
    public function getAccount(Request $request)
    {
        // $user = JWTAuth::parseToken()->toUser();

        $account = Account::find($request->id);

        if (count($account) > 0) {
            $user = $account->user;
            $account->loadTransactions();
            $account->loadBalanceHistory(2017);
            $account->loadAccountAllocation();
            return response()->json([
                "account" => $account,
                "user" => $user
            ], 201);
        } else {
            return ApiJson::ApiResponse('Account not found', 422);
        }
    }

    public function createAccount(Request $request)
    {
        //get user ID from token User
        $user = JWTAuth::parseToken()->toUser();
        if (count($user) < 1) {
            return ApiJson::ApiErrorResponse('Unauthorized', 401);
        }

        $account = new Account;
        $account->user_id = $user->id;
        $account->type = $request->type;
        $account->name = $request->name;
        $account->status = 'active';

        try {
            $account->save();
            return ApiJson::ApiResponse('Account Created', 201);
        } catch (Exception $e) {
            if ($e instanceof \Illuminate\Database\QueryException) {
                throw new ApiException($e, 'Failed to Create Account', 500);
            }
        }
    }

    public function deleteAccount(Request $request)
    {
        $account = Accounts::find($request->id);

        if ($account) {
            $account->status = 'inactive';
            $account->save();
            return ApiJson::ApiResponse($account, 201);
        } else {
            return ApiJson::ApiResponse('Account not found', 422);
        }
    }

    public function Accounts(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        $accounts = $user->accounts;

        if ($accounts) {
            foreach ($accounts as $account) {
                $account->loadTransactions();
                $account->loadBalanceHistory(2017);
                $account->loadAccountAllocation();
            }
            return response()->json([
                "user" => $user,
                "accounts" => $accounts
                ], 201);
        }
        return ApiJson::ApiResponse("Accounts not found", 422);
    }

    public function AccountsList(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        if ($user->admin) {
            $accounts = Accounts::all()->with('users');
        }
    }
}
