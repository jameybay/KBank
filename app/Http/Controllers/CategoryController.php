<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\CategoryGroup;
use App\Models\TransactionCategory;
use DB;
use App\Classes\ApiJson;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class CategoryController extends Controller
{
    private function loadCategories($user)
    {
        $categories = CategoryGroup::where('global', 'true')
                              ->orwhere('user_id', $user->id)
                              ->get();
        $id = $user->id;
        $categories->load(['transactioncategories' => function ($query) use ($id) {
            $query->where('user_id', $id)
                  ->orwhere('global', 'true');
        }]);
        return $categories;
    }
    public function CategoryGroupsList(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        if(!empty($user)) {
            $categorylist = $this->loadCategories($user);

            return response()->json([
                'categories' => $categorylist
            ], 201);
        }
        return ApiJson::ApiResponse("Unauthorized", 421);
    }

    public function UserTransactionCategories(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        if(!empty($user)) {
            $categories = $this->loadCategories($user);


            return response()->json([
                'categories' => $categories
            ], 201);
        }
        return ApiJson::ApiResponse("Unauthorized", 421);

    }

    public function Create(Request $request)
    {
        $user = JWTAuth::parseToken()->toUser();

        if(!empty($user)) {
            if ($request->group == 0) {
                $category = new CategoryGroup;
            }
            else {
                $category = new TransactionCategory;
                $category->categorygroup_id = $request->group;
            }
            $user->admin == 'true' ? $category->global = 'true' :
                                     $category->global = 'false';
            $category->user_id = $user->id;
            $category->name = $request->name;
            $category->comments = $request->comments;
            $category->taxreport = $request->tax_report;

            try {
                $category->save();
            }
            catch (Exception $e) {
                return ApiJson::ApiResponse("Could not execute query", 500);
            }
            return ApiJson::ApiResponse("Success", 201);
        }
        return ApiJson::ApiResponse("Unauthorized", 421);
    }

    public function Delete(Request $request)
    {

    }

}
