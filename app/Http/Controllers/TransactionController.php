<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exceptions;
use Illuminate\Support\Str;
use App\Exceptions\ApiException;
use App\Models\Account;
use App\Models\Transaction;
use App\Classes\ApiJson;
use JWTAuth;
use DB;

class TransactionController extends Controller
{
    /**
     * Get Transaction by ID
     * @param  Request $request [description]
     * @return Transaction
     */
    public function getTransaction(Request $request)
    {
        $transaction = Transaction::find($request->id);

        if ($transaction) {
            return ApiJson::ApiResponse($transaction, 201);
        } else {
            return ApiJson::ApiResponse('Transaction not found', 422);
        }
    }

    /**
     * Create a new Transaction
     * @param  Request $request [description]
     * @return ApiJson status
     */
    public function create(Request $request)
    {
        // Check if account exists first
        $account = Account::find($request->account_id);
        if ($account) {
            $transaction = new Transaction;

            $transaction->type = $request->type;
            $transaction->check_number = $request->check_number;
            $transaction->account_id = $request->account_id;
            $transaction->source = $request->source;
            $transaction->source_id = $request->source_id;
            $transaction->category_id = $request->category_id;
            $transaction->comments = $request->comments;

            switch ($transaction->type) {
                case 'deposit':
                    $transaction->amount = $request->amount;
                    break;

                case 'cash_withdraw':
                case 'check_withdraw':
                case 'fine':
                case 'debit':
                    $transaction->amount = - $request->amount;
                    break;

                default:
                    break;
            }

            try {
                $transaction->save();
                return ApiJson::ApiResponse('Transaction Completed', 201);
            } catch (Exception $e) {
                if ($e instanceof \Illuminate\Database\QueryException) {
                    throw new ApiException($e, 'Transaction Failed', 401);
                }
            }
        } else {
            return ApiJson::ApiResponse("Account not Found", 422);
        }
    }

    private function fundsTransfer($funds_origination, $funds_destination, $amount, $comments)
    {
        $success = true;

        $transaction_to = new Transaction;
        $transaction_from = new Transaction;

        $transaction_from->type = 'transfer';
        $transaction_from->amount = - $amount;
        $transaction_from->comments = $comments;
        $transaction_from->account_id = $funds_origination->id;
        $transaction_from->source = "Account " . $funds_origination->id;
        $transaction_from->source_id = $funds_origination->id;

        $transaction_to->type = 'deposit';
        $transaction_to->amount = $amount;
        $transaction_to->comments = $comments;
        $transaction_to->account_id = $funds_destination->id;
        $transaction_to->source = "Account " . $funds_origination->id;
        $transaction_to->source_id = $funds_origination->id;

        try {
            $transaction_from->save();
            $transaction_to->save();

            $funds_origination->balance -= $amount;
            $funds_destination->balance += $amount;
            $funds_origination->save();
            $funds_destination->save();
        } catch (Exception $e) {
            $success = false;
        }
        return $success;
    }

    /**
     * Transfer funds from one account to another for the same user
     * @param Request $request [description]
     * @return ApiJson status
     */
    public function Transfer(Request $request)
    {
        $account_from = Account::find($request->account_from);
        $account_to = Account::find($request->account_to);
        $user = JWTAuth::parseToken()->toUser();

        if (($user == $account_from->user) &&
            ($user == $account_to->user)) {
            if ($this->fundsTransfer($account_from, $account_to, $request->amount, $request->comments)) {
                return ApiJson::ApiResponse('Transaction Completed', 201);
            }
        }
        return ApiJson::ApiResponse('Transaction Failed', 422);
    }

    /**
     * Levy a charge against an account and transfer an amount from the account
     * to the Kbank general account.
     * @param Request $request [description]
     */
    public function Fine(Request $request)
    {
        $admin = DB::table('users')->where('name', 'admin')->first();
        $user = JWTAuth::parseToken()->toUser();

        $account_from = Account::find($request->account_from);
        $account_to = Account::->where('name', 'KBank Main Account')->first();
        $user = JWTAuth::parseToken()->toUser();

        if (($user == $account_from->user) && ($admin == $account_to->user)) {
            if ($this->fundsTransfer($account_from, $account_to, $request->amount, $request->comments)) {
                return ApiJson::ApiResponse('Transaction Completed', 201);
            }
        }
        return ApiJson::ApiResponse('Transaction Failed', 422);
    }

    /**
     * Get the count for all transactions
     * @param  Request $reqest
     * @return integer count
     * @return ApiJson status
     */
    public function getTransactionCount(Request $request)
    {
        try {
            $count = DB::table('transactions')->count();
            $firstId = DB::table('transactions')->first();
        } catch (Exception $e) {
            return ApiJson::ApiErrorResponse('Internal Error', '500');
        }

        return ApiJson::ApiResponse([
            'count' => $count,
            'first_record' => $firstId
        ], 200);
    }

    /**
     * Get a range of Transactions
     * @param Request $request contains range parameters
     * @param integer begin begining record
     * @param integer end ending record
     * @return Transactions array
     * @return ApiJson status
     */
    public function GetTransactions(Request $request)
    {
        try {
            $count = DB::table('transactions')->count();
            $firstId = DB::table('transactions')->first();
            $transactions = DB::table('transactions')
                ->whereBetween('id', [
                    $request->begin,
                    $request->end
                ])
                // ->orderBy('created_at', 'desc')
                ->get();
        } catch (Exception $e) {
            return ApiJson::ApiErrorResponse('Internal Error', '500');
        }

        if (count($transactions)) {
            return ApiJson::ApiResponse([
                'transactions' => $transactions,
                'count' => $count,
                'first_record' => $firstId
            ], 200);
        } else {
            return ApiJson::ApiResponse('No Transactions found', 404);
        }
    }
}
