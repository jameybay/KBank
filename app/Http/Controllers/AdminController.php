<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Transaction;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth-admin');
    }

    // Deprecated use UserController userlist
    public function getUserlist(Request $Request)
    {
        $userlist = User::all();

        return response()->json([
            "userlist" => $userlist
        ], 201);
    }

    // Deprecated use TransactionsController transactionslist
    public function getTransactionslist(Request $Request)
    {
        $transactionslist = Transaction::orderBy('created_at', 'asc')->get();

        return response()->json([
            "transactionslist" => $transactionslist
        ], 201);
    }
}
