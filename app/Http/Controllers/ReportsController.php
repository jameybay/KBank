<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class ReportsController extends Controller
{
    public function NegativeBalances(Request $request)
    {
        //join with users to get name
        $accounts = DB::table('accounts')
                            ->join('users', 'accounts.user_id', 'users.id')
                            ->select(
                                'accounts.id',
                                'users.first_name',
                                'users.last_name',
                                'accounts.balance',
                                'accounts.updated_at'
                            )
                            ->where('accounts.balance', '<', '0')
                            ->orderBy('accounts.balance', 'asc')
                            ->get();

        return response()->json([
            "report_title" => "Negative Balances",
            "headers" => ["ID", 'First Name', 'Last_name', "Balance", "Last Transaction"],
            "records" => $accounts
        ], 200);
    }

    public function ReportNewestUsers(Request $request)
    {
        $users = DB::table('users')
                            ->select('first_name', 'last_name', 'email', 'created_at')
                            ->where('admin', '=', 'false')
                            ->latest()
                            ->take(10)
                            ->orderBy('created_at', 'desc')
                            ->get();

        return response()->json([
            "report_title" => "Newest Users",
            "headers" => ["First Name", "Last Name", "Email", "Start Date"],
            "records" => $users
        ], 200);
    }

    public function ReportMostActiveAccounts(Request $request)
    {
        switch ($request->period) {
            case 'month':
                $report_period = Carbon::now()->subMonths(1)->firstOfMonth();
                break;
            case 'halfyear':
                $report_period = Carbon::now()->subMonths(6)->firstOfMonth();
                break;
            case 'year':
                $report_period = Carbon::now()->subYears(1)->firstOfMonth();
                break;
            default:
                $report_period = Carbon::now()->subYears(1);
                break;
        }
        $transactions = DB::table('transactions')
                            ->select(DB::raw('count(*) as Transactions, account_id, created_at'))
                            ->where('created_at', '>=', $report_period)
                            ->groupBy("account_id")
                            ->get();

        return response()->json([
            "report_title" => "Most Active Accounts from " . $report_period->toDateString(),
            "headers" => ["Transactions", "ID", "Date"],
            "records" => $transactions
        ], 200);
    }

    public function NewUsersPerMonth(Request $request)
    {
        $users = DB::table('users')
                            ->select(DB::raw("count(*) as users,
                            strftime('%m', created_at) as t_month,
                            strftime('%Y', created_at) as t_year"))
                            ->groupBy("t_month", "t_year")
                            ->orderBy("users", "desc")
                            ->get();

        return response()->json([
            "report_title" => "New Users per Month",
            "headers" => ["Users", "Month", "Year"],
            "records" => $users
        ], 200);
    }
}
