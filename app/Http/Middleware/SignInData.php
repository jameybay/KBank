<?php

namespace App\Http\Middleware;

use Closure;
use Jenssegers\Agent\Agent;
use App\Models\SigninData as Data;

class SignInData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $agent = new Agent();
        $data = new Data();

        $data->platform = $agent->platform();
        $data->platformversion = $agent->version($data->platform);

        $data->browser = $agent->browser();
        $data->browserversion = $agent->version($data->browser);

        $data->ipaddress = $request->ip();
        $data->useremail = $request->email;

        $data->save();

        return $next($request);
    }
}
