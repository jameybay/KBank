<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use App\Models\User;


class AuthenticateAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //get user from token
        $user = JWTAuth::parseToken()->toUser();
        //check if admin
        if(!empty($user)) {
            if($user->admin == 'true') {
                //yes continue
                return $next($request);
            }
        }
        //else abort
        abort(404);
    }
}
