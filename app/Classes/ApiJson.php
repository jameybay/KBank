<?php

namespace App\Classes;

use Illuminate\Support\MessageBag;
use App\Classes\HttpStatusCodes;


/**
 * ApiResponse
 */
class ApiJson
{
    public static function ApiErrorResponse($message, $status_code)
    {
        // Array
        if(is_array($message)) {
            return response()->json($message, $status_code);
        }

        // if(is_array($message)) {
        //     return response()->json([
        //         "size" => sizeof($message),
        //         "message" => $message,
        //         "description" => HttpStatusCodes::GetDescription($status_code)
        //         ], $status_code);
        // }
        // String
        if(is_string($message)) {
            return response()->json([
                "message" => $message,
                "description" => HttpStatusCodes::GetDescription($status_code)
            ], $status_code);
        }
        // MessageBag
        if($message instanceof MessageBag) {
            return response()->json($message->all(), $status_code);
        }

    }

    public static function ApiResponse($message, $status_code)
    {
        // Array
        if(is_array($message)) {
            return response()->json([
                "size" => sizeof($message),
                "message" => $message,
                "description" => HttpStatusCodes::GetDescription($status_code)
                ], $status_code);
        }
        // Object
        if(is_object($message)) {
            return response()->json([
                self::getclassname(get_class($message)) => $message,
                "description" => HttpStatusCodes::GetDescription($status_code)
            ], $status_code);
        }

        return response()->json([
            "message" => $message,
            "description" => HttpStatusCodes::GetDescription($status_code)
        ], $status_code);
    }

    private static function getclassname($namespaceclass)
    {
        if($pos = strrpos($namespaceclass, '\\'))
            return substr($namespaceclass, $pos +1);

        return $pos;
    }
}
