<?php

namespace App\Exceptions;

use Exception;


/**
 * ApiException
 */
class ApiException extends Exception
{
    private $innerException;
    private $apimessage;
    private $status_code;

    function __construct(Exception $exception, $msg, $status)
    {
        $this->innerException = $exception;
        $this->apimessage = $msg;
        $this->status_code = $status;
    }

    public function getInnerException()
    {
        return $this->innerException;
    }

    public function getApiMessage()
    {
        return $this->apimessage;
    }

    public function getApiMessageJson()
    {
        return json_encode([
            'message' => $this->apimessage
        ]);
    }

    public function getStatusCode()
    {
        return $this->status_code;
    }
}
