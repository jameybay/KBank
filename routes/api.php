<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
*/

// Global
Route::post('/signup', "UserController@SignUp");
Route::post('/signin', "UserController@SignIn")->middleware('signin-data');

// User
Route::post('/user/create', "UserController@CreateUser");
Route::get('/user/{id}/info', "UserController@getUser")->middleware('auth-jwt');
Route::get('/user', "UserController@getAuthUser")->middleware('auth-jwt');
Route::post('/user/changepassword', "UserController@ChangePassword")->middleware('auth-jwt');
Route::post('/user/changeinfo', "UserController@ChangeInfo")->middleware('auth-jwt');

// Accounts
Route::get('/user/account/{id}', 'AccountsController@getAccount')->middleware('auth-jwt');
Route::post('/user/account/create', 'AccountsController@createAccount')->middleware('auth-jwt');
Route::get('/user/accounts', "AccountsController@Accounts")->middleware('auth-jwt');
Route::post('/user/account/{id}/delete', 'AccountsController@deleteAccount')->middleware('auth-jwt');

// Transactions
Route::get('/user/transaction/{id}', 'TransactionController@getTransaction')->middleware('auth-jwt');
Route::post('/user/transaction/create', 'TransactionController@create')->middleware('auth-jwt');
Route::post('/user/transaction/transfer', 'TransactionController@Transfer')->middleware('auth-jwt');
// Route::get('/transactions/{user_id}', 'TransactionController@getTransactions')->middleware('auth-jwt');

// Messages
Route::get('/user/messages/count', 'MessageController@MessageCount')->middleware('auth-jwt');
Route::post('/admin/message/create', 'MessageController@CreateMessage')->middleware('auth-admin');
Route::get('/user/messages', 'MessageController@getMessages')->middleware('auth-jwt');
Route::get('/user/message/{id}', 'MessageController@getMessage')->middleware('auth-jwt');
Route::get('/user/message/{id}/mark', 'MessageController@MarkMessage')->middleware('auth-jwt');
Route::get('/user/message/{id}/delete', 'MessageController@DeleteMessage')->middleware('auth-jwt');

// Categories
Route::get('/admin/category/{id}/delete', 'CategoryController@Delete')->middleware('auth-admin');
Route::post('/user/category/create', 'CategoryController@Create')->middleware('auth-jwt');
Route::get('/user/category/list', 'CategoryController@CategoryGroupsList')->middleware('auth-jwt');
Route::get('/user/categories', 'CategoryController@UserTransactionCategories')->middleware('auth-jwt');

// Administrator
// Route::get('/admin/transactionslist', 'AdminController@getTransactionslist')->middleware('auth-admin');
Route::post('/admin/users/list', 'UserController@getUsers')->middleware('auth-admin');
Route::get('/admin/transactions/count', 'TransactionController@getTransactionCount')->middleware('auth-admin');
Route::post('/admin/transactions/list', 'TransactionController@GetTransactions')->middleware('auth-admin');

// Reports
Route::get('/admin/reports/mostactiveaccounts/{period}', 'ReportsController@ReportMostActiveAccounts')->middleware('auth-admin');
Route::get('/admin/reports/newusers', 'ReportsController@NewUsersPerMonth')->middleware('auth-admin');
Route::get('/admin/reports/newestusers', 'ReportsController@ReportNewestUsers')->middleware('auth-admin');
Route::get('/admin/reports/negativebalances', 'ReportsController@NegativeBalances')->middleware('auth-admin');
