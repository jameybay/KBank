<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/app.css">
        <title>KBank</title>
    </head>
    <body>
        <section class="hero is-primary">
            <div class="hero-body">
                <div class="container">
                     <h1 class="title">KBank</h1>
                     <h2 class="subtitle">
                         Your personal finacial package for those just learning about finances
                     </h2>
                </div>
            </div>
            <div class="hero-foot">
                @include('partials.landingnavbar')
            </div>
        </section>
        @yield('main')
        <script src="/js/app.js"></script>
    </body>
</html>
