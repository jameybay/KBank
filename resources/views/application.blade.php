<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/app.css">
        <title>KBank</title>

    </head>
    <body>
        <div id="app">

            <navbar></navbar>

            <router-view></router-view>

        </div>

        <script src="/js/app.js"></script>

    </body>
</html>
