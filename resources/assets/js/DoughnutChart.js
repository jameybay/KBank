// import VueCharts from 'vue-chartjs';
import { Doughnut, mixins } from 'vue-chartjs';
const { reactiveProp } = mixins;

export default Doughnut.extend({
    mixins: [reactiveProp],

    props: {
        showLegend: {
            type: String,
            required: false,
            default: true
        }
    },

    data: () => ({
        options: {
            cutoutPercentage: 65,
            // maintainAspectRatio: true,
            responsive: true,
            legend: {
                // display: this.showLegend,
                position: 'left',
                labels: {
                    boxWidth: 20,
                    fontSize: 10,
                    fontColor: 'green'
                }
            }
        },
    }),

    mounted () {
        this.options.legend.display = this.showLegend === 'true';
        Chart.defaults.global.defaultFontColor = 'red';
        this.renderChart(this.chartData, this.options);
    }
});
