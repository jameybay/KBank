/**
 * import Vue
 */

import Vue from 'vue';
window.Vue = Vue;

import axios from 'axios';
window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import formatCurrency from './components/helpers/formatCurrency';
window.formatCurrency = formatCurrency;

export const EventBus = new Vue();
window.EventBus = EventBus;

import navbar from './components/common/NavBar';
Vue.component('navbar', navbar);

import 'vue-awesome/icons';
import Icon from 'vue-awesome/components/Icon';
Vue.component('icon', Icon);



// TODO import icons
