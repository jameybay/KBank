// import VueCharts from 'vue-chartjs';
import { Line, mixins } from 'vue-chartjs';
const { reactiveProp } = mixins;

export default Line.extend({
    mixins: [reactiveProp],

    data: () => ({
        options: {
            width: 360,
            height: 360,
            maintainAspectRatio: true,
            responsive: true,
            legend: {
                labels: {
                    fontColor: 'green'
                }
            }
        },
    }),

    mounted () {
        Chart.defaults.global.defaultFontColor = 'red';
        this.renderChart(this.chartData, this.options);
    }
});
