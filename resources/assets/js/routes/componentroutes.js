import VueRouter from 'vue-router';


let routes = [
    /**
     * Default Page - SignIn
     * @type {String}
     */
    {
        path: '*',

        component: require('./../components/common/Signin')
    },
    /**
     * About Page
     * @type {String}
     */
    {
        path: '/about',

        component: require('./../components/common/About')
    },
    /**
     * Users Home Page
     * @type {String}
     */
    {
        name: 'userhome',

        path: '/user',

        component: require('./../components/user/Home'),

        children: [
            // {
            //     path: '/user/*',
            //
            //     component: require('./../components/user/Dashboard')
            // },
            {
                name: 'useraccounts',

                path: '/user/accounts',

                component: require('./../components/user/Accounts')
            },
            {
                name: 'accountdetails',

                path: '/user/account/:id',

                component: require('./../components/user/AccountDetails')
            },
            {
                name: 'createtransaction',

                path: '/user/transaction',

                component: require('./../components/user/CreateTransaction')
            },
            {
                name: 'createaccount',

                path: '/user/account/create',

                component: require('./../components/user/CreateAccount')
            },
            {
                name: 'transfer',

                path: '/user/transfer',

                component: require('./../components/user/Transfer')
            },
            {
                name: 'messages',

                path: '/user/messages',

                component: require('./../components/user/Messages')
            },
            {
                name: 'dashboard',

                path: '/user/dashboard',

                component: require('./../components/user/Dashboard'),

                children: [
                    {
                        name: 'expendinturechart',

                        path: '/user/dashboard/expendinture/:id',

                        component: require('./../components/common/ExpenditureChart')
                    },
                    {
                        name: 'incomechart',

                        path: '/user/dashboard/income/:id',

                        component: require('./../components/common/IncomeChart')
                    },
                ]
            },
        ]
    },
    /**
     * Profile parent
     * @type {String}
     */
    {
        name: 'profile',

        path: '/user/profile',

        component: require('./../components/user/ProfileMenu'),

        children: [
            {
                name: 'userprofile',

                path: '/user/profile/profile',

                component: require('./../components/user/UserProfile')
            },
            {
                name: 'changepassword',

                path: '/user/profile/changepassword',

                component: require('./../components/user/ChangePassword')
            },
            {
                name: 'changedata',

                path: '/user/profile/changedata',

                component: require('./../components/user/ChangeUserInfo')
            },
            {
                name: 'usercreatecategory',

                path: '/user/profile/category/create',

                component: require('./../components/common/CreateCategory')
            }
        ]
    },

    /**
     * Create a User with all information
     * @type {String}
     */
    {
        name: 'createuser',

        path: '/new',

        component: require('./../components/common/NewUser')
    },
    /**
     * Signup Page
     * @type {String}
     */
    {
        name: 'signup',

        path: '/signup',

        component: require('./../components/common/Signup')
    },
    /**
     * Signin Page
     * @type {String}
     */
    {
        name: 'signin',

        path: '/signin',

        component: require('./../components/common/Signin')
    },
    /**
     * Signout
     * delete localStorage
     * @type {String}
     */
    {
        name: 'signout',

        path: '/signout',

        component: require('./../components/common/Signout')
    },
    /**
     * Admin Home Page
     *
     * Children
     * Userlist - List of users for Admin
     * UserInfo - Admin information and operations for single user
     * TransactionsList - List all Transactions
     *
     * @type {String}
     */
    {
        name: 'adminhome',

        path: '/admin',

        component: require('./../components/admin/Home'),

        children: [
            {
                name: 'admindashboard',

                path: '/admin/',

                component: require('./../components/admin/Dashboard')
            },
            {
                name: 'userlist',

                path: '/admin/userlist/{url}',

                component: require('./../components/admin/UserList'),

                props: true
            },
            {
                name: 'userinfo',

                path: '/admin/userinfo/:id',

                component: require('./../components/admin/UserInfo')
            },
            {
                name: 'transactionslist',

                path: '/admin/transactionslist',

                component: require('./../components/admin/TransactionsList')
            },
            {
                name: 'accountinfo',

                path: '/admin/account/:id',

                component: require('./../components/user/Account')
            },
            {
                name: 'createmessage',

                path: '/admin/message/create',

                component: require('./../components/admin/CreateMessage')
            },
            {
                name: 'createcategory',

                path: '/admin/category/create',

                component: require('./../components/common/CreateCategory')
            },
            {
                name: 'genericreport',

                path: '/admin/reports/:report_url',

                component: require('./../components/admin/Reports/GenericReport'),

                props: true
            },
        ]
    },
];

export default new VueRouter ({
    routes,
    linkActiveClass: 'is-active',
    // mode: history
});
