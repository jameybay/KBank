
module.exports = {
    data: () => ({
        user: '',
    }),

    mounted () {
        const token = localStorage.getItem('token');
        const header = 'Bearer ' + token;
        axios.get('/api/user',
            {
                headers: {
                    'Authorization' : header
                }
            })
            .then(response => {
                this.user = response.data.user;
            })
            .catch(error => {
                this.$router.replace({name: 'signin'});
        });
    },

}
