// Date formatting mixin object
module.exports = {
    data () {
        return {
            months: [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December',
            ],

            monthsabbr: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec',
            ],

            days: [
                'Sunday',
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday'
            ],

            daysabbr: [
                'Sun',
                'Mon',
                'Tues',
                'Wed',
                'Thu',
                'Fri',
                'Sat'
            ],
        }
    },

    methods: {
        monthString: function (date) {
            let d = new Date(date);
            return this.months[d.getMonth()];
        },
        monthStringAbbr: function (date) {
            let d = new Date(date);
            return this.monthsabbr[d.getMonth()];
        },
        monthNumber: function (date) {
            let d = new Date(date);
            return d.getMonth();
        },
        dayString: function (date) {
            let d = new Date(date);
            return this.days[d.getDay()];
        },
        dayStringAbbr: function (date) {
            let d = new Date(date);
            return this.daysabbr[d.getDay()];
        },
        dayNumber: function (date) {
            let d = new Date(date);
            let day = d.getDate();
            return day.toString().length === 1 ? '0' + day : day;
        },
        yearNumber: function (date) {
            let d = new Date(date);
            return d.getFullYear();
        },
        timeNumber: function (date) {
            let d = new Date(date);
            let t = d.getHours();
            t += ':';
            t += d.getMinutes();
            return t;
        },

    }
}
