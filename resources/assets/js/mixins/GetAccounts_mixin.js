
module.exports = {
    data: () => ({
        user: '',
        accounts: [],
    }),

    mounted () {
        const token = localStorage.getItem('token');
        const header = 'Bearer ' + token;
        axios.get('/api/user/accounts',
            {
                headers: {
                    'Authorization' : header
                }
        })
            .then(response => {
                this.user = response.data.user;
                this.accounts = response.data.accounts;
            })
            .catch(error => {
                console.log(error);
            })
    },
}
